# Text classification with tensorflow 2

This is a test repository for text classification with tensorflow 2. It is used for training, evaluating, loading and predicting.
It also contains TensorBoard to get a visual representation of the trained models.