import pandas as pd
import glob
import os
import string

import tensorflow as tf

import pickle

NUM_WORDS = 8000
SEQ_LEN = 128
EMBEDDING_SIZE=128
BATCH_SIZE=128
EPOCHS=10
THRESHOLD=0.5


loaded_model=tf.keras.models.load_model('logs\\512-layer_size-1-dense-512-batch-1583607269_876.h5')

with open('tokenizer.pickle', 'rb') as f:
    loaded_tokenizer = pickle.load(f)

def prepare_predict_data(tokenizer, reviews):
  seqs = tokenizer.texts_to_sequences(reviews)
  seqs = tf.keras.preprocessing.sequence.pad_sequences(seqs, maxlen=SEQ_LEN, padding="post")
  return seqs

my_reviews=['this movie was awesome',
           'this movie was the worst movie ive ever seen',
           'i hated everything about this movie',
           'this is my favorite movie of the year',
            'bad',
            'good',
            'not so good',
            'I have seen better',
            'So far I have not seen a single movie that is better',
            'really bad',
            'brilliant',
            'Watching this film was like reliving the jobs I had in my 20s - waiting for 5 oclock, looking busy, trying to impress people who did not give two hoots about you, and the dreams...ah, the dreams...<br /><br />This is a very believable study of working life for the bottom rung female. How they band together, then fall apart. It is ultimately a sad story with some hope for 2 of the characters. But it is as close to life as it gets.<br /><br />Wonderful performances by all 4 actresses. And small roles by some big character actors make this a good film.'
            'The four leads are very effective as the disaffected temps, pariahs in a generic office. They are ably supported by veterans such as Stanley De Santis and Bob Balaban. For its first forty minutes, Clockwatchers effectively creates a Twilight-Zonish office hierarchical atmosphere with our heroines as the office pariahs. Then it spends its last hour expounding upon the fragile and transitory nature of office friendships amidst corporate backstabbing for advancement and survival -- especially among temps. After the first hour, it is the viewer who is the clockwatcher as it winds down with no additional insights, humor, or surprises. I would give the first half a 7, and the second half a 1, which leaves me with a 4.']
my_seqs = prepare_predict_data(loaded_tokenizer, my_reviews)

preds= loaded_model.predict(my_seqs)
print(preds)

pred_df = pd.DataFrame(columns=['text', 'sent'])
pred_df['text'] = my_reviews
pred_df['sent'] = preds

pred_df['sent'] = pred_df['sent'].apply(lambda x: 'pos' if x > THRESHOLD else 'neg')

print(pred_df)

