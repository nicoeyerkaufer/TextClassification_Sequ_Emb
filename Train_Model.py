import pandas as pd
import glob
import os
import string
import spacy
import tensorflow as tf
import time
from tensorflow.keras.callbacks import TensorBoard

NUM_WORDS = 10000 #10000
SEQ_LEN = 256 #256
EMBEDDING_SIZE=128 #128

EPOCHS=50
THRESHOLD=0.5
EMBEDDING_SIZE_RNN = 64

layer_sizes=[127,257,513]
dense_layers = [1]
BATCH_SIZE=[127,257,513]

num_filters=[32, 64, 128]
kernel_size=[3, 5, 7]
 #512

CALLBACK_YES_NO = True
CLEANUP_TEXT = False

import matplotlib.pyplot as plt



# Define function to cleanup text by removing personal pronouns, stopwords, and puncuation
def cleanup_text(docs, logging=False):
    texts = []
    counter = 1
    for doc in docs:
        if counter % 1000 == 0 and logging:
            print("Processed %d out of %d documents." % (counter, len(docs)))
        counter += 1
        doc = nlp(doc, disable=['parser', 'ner'])
        tokens = [tok.lemma_.lower().strip() for tok in doc if tok.lemma_ != '-PRON-']
        tokens = [tok for tok in tokens if tok not in stopwords and tok not in punctuations]
        tokens = ' '.join(tokens)
        texts.append(tokens)
    return pd.Series(texts)

def plot_graphs(history, metric):
  plt.plot(history.history[metric])
  plt.plot(history.history['val_'+metric], '')
  plt.xlabel("Epochs")
  plt.ylabel(metric)
  plt.legend([metric, 'val_'+metric])
  plt.show()

def create_model(dense_layer,layer_size):
  #model = tf.keras.Sequential([tf.keras.layers.Embedding(NUM_WORDS, EMBEDDING_SIZE),
  #                             #tf.keras.layers.Conv1D(128, 7, activation='relu'),
  #                             tf.keras.layers.GlobalAveragePooling1D(),
  #                             tf.keras.layers.Dense(16, activation='relu'),
  #                             #tf.keras.layers.Dropout(0.2),
  #                             tf.keras.layers.Dense(1, activation='sigmoid')])#
  model = tf.keras.Sequential()
  model.add(tf.keras.layers.Embedding(NUM_WORDS, EMBEDDING_SIZE))
  model.add(tf.keras.layers.GlobalAveragePooling1D())
  for l in range(dense_layer):
      model.add(tf.keras.layers.Dense(layer_size, activation='relu'))
      model.add(tf.keras.layers.Dropout(0.15))

  model.add(tf.keras.layers.Dense(1, activation='sigmoid'))
  model.summary()
  # binary_crossentropy
  model.compile(optimizer='adam',
                loss='binary_crossentropy',
                metrics=['accuracy'])
  return model

def create_model_Dense():
  model = tf.keras.Sequential([tf.keras.layers.Embedding(NUM_WORDS, EMBEDDING_SIZE),
                               tf.keras.layers.Flatten(),
                               tf.keras.layers.Dense(10,  activation='relu'),
                               tf.keras.layers.Dense(1, activation='sigmoid')])
  model.summary()
  model.compile(optimizer='adam',
                loss='binary_crossentropy',
                metrics=['accuracy'])
  return model

def create_model_RNN():
    model = tf.keras.Sequential([tf.keras.layers.Embedding(NUM_WORDS, EMBEDDING_SIZE_RNN),
         tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(128, return_sequences=True)),
         tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64)),
         tf.keras.layers.Dense(128, activation='relu'),
         tf.keras.layers.Dropout(0.5),
         tf.keras.layers.Dense(1)
     ])


    model.summary()
    model.compile(loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              optimizer=tf.keras.optimizers.Adam(1e-4),
              metrics=['accuracy'])
    return model


def get_dfs(start_path):

  df = pd.DataFrame(columns=['text', 'sent'])
  text = []
  sent = []
  for p in ['pos','neg']:
    path=os.path.join(start_path, p)
    files = [f for f in os.listdir(path)
             if os.path.isfile(os.path.join(path,f))]
    for f in files:
      with open (os.path.join(path, f), "r", encoding="utf8") as myfile:
        # replace carriage return linefeed with spaces
        text.append(myfile.read()
                    .replace("\n", " ")
                    .replace("\r", " "))
        # convert positive reviews to 1 and negative reviews to zero
        sent.append(1 if p == 'pos' else 0)

  df['text']=text
  df['sent']=sent
  #This line shuffles the data so you don't end up with contiguous
  #blocks of positive and negative reviews
  df = df.sample(frac=1).reset_index(drop=True)
  return df

train_df = get_dfs("aclImdb/train/")
test_df = get_dfs("aclImdb/test/")

print(train_df)
print(test_df)

train_X = train_df['text']
test_X = test_df['text']
if CLEANUP_TEXT == True:
    # Clean text before feeding it to spaCy
    nlp = spacy.load("en_core_web_sm")
    punctuations = string.punctuation
    stopwords = nlp.Defaults.stop_words

    train_X = cleanup_text(train_df['text'])
    test_X = cleanup_text(test_df['text'])

#create tokenizer for our data
tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=NUM_WORDS, oov_token='<UNK>')
tokenizer.fit_on_texts(train_X)


#convert text data to numerical indexes
train_seqs=tokenizer.texts_to_sequences(train_X)
test_seqs=tokenizer.texts_to_sequences(test_X)


#pad data up to SEQ_LEN (note that we truncate if there are more than SEQ_LEN tokens)
train_seqs=tf.keras.preprocessing.sequence.pad_sequences(train_seqs, maxlen=SEQ_LEN, padding="post")
test_seqs=tf.keras.preprocessing.sequence.pad_sequences(test_seqs, maxlen=SEQ_LEN, padding="post")

print(train_seqs)
print(test_seqs)


#model = create_model_RNN()
#model = create_model_Dense()

history = 0
for dense_layer in dense_layers:
    for layer_size in layer_sizes:
        for batch in BATCH_SIZE:
            NAME = f"{layer_size}-layer_size-{dense_layer}-dense-{batch}-batch-{int(time.time())}"
            model = create_model(dense_layer, layer_size)
            if CALLBACK_YES_NO == True:
                es = tf.keras.callbacks.EarlyStopping(monitor='val_accuracy',
                                                      mode='max')
                tensorboard = TensorBoard(log_dir='logs\\{}'.format(NAME))
                callbacks=[es, tensorboard]
                history = model.fit(train_seqs,
                                    train_df['sent'].values,
                                    batch_size=batch,
                                    epochs=EPOCHS,
                                    validation_split=0.1,
                                    #validation_data=test_seqs,
                                    #validation_steps=30,
                                    callbacks=callbacks
                                    )
            else:
                history = model.fit(train_seqs, train_df['sent'].values,
                                    batch_size=batch,
                                    epochs=EPOCHS,
                                    validation_split=0.1)

            loss, accuracy = model.evaluate(test_seqs, test_df['sent'].values)
            print("Loss: ", loss)
            print("Accuracy: ", accuracy)

            accuracy_id = str(accuracy)
            accuracy_id = accuracy_id[2:5]
            model.save('logs\\' + NAME +'_'+accuracy_id +'.h5')



plot_graphs(history, 'accuracy')
plot_graphs(history, 'loss')



import pickle
model.save('model'+ accuracy_id +'.h5')
# saving
with open('tokenizer.pickle', 'wb') as handle:
    pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)

del model
del tokenizer

